
from pattern.web import Wikipedia
from stemming.porter2 import stem
from curses.ascii import isalpha


def get_article(article):
    art = article.string
    art = art.lower().strip().split()

    for i in range(len(art)):
        art[i] = art[i].encode('ascii','ignore')
        art[i] = art[i].translate(None, '@$/#.-:&*+=[]?!(){},''">_<;%')
        art[i] = stem(art[i])
        #if art[i].isdigit(): # convert numbers to token 'number'?
        #    art[i] = 'numb'           
    art.sort()

    for a in art:
        a = unicode(a)

    return art