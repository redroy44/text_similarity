
import math
from pattern.web import Wikipedia
import pprint
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy
import wiki_processing as wiki
from stemming.porter2 import stem


def filter_text(article):
    text = []
    for i in range(len(article)):
        article[i] = stem(article[i])
        
        if (article[i].isalnum() and len(article[i])> 2) and not (article[i].isdigit() and len(article[i])> 9):
            text.append(article[i])
            #print article[i]
    return text
   
#def cos_similarity(vector1, vector2):   
#     a = numpy.array(vector1)
#     b = numpy.array(vector2)
#     a_norm = numpy.linalg.norm(a)
#     b_norm = numpy.linalg.norm(b)
#     dot_product = numpy.dot(a,b)
#     
#     return dot_product / (a_norm * b_norm)

def get_link_list(art1, art2):
    list1 = art1.links
    list2 = art2.links
    print len(list1)
    print len(list2)
 
    link_list = list(set(list1).intersection(list2))
    print link_list

    print 'number of common links: ', len(link_list)
    
    return link_list
   
def get_feature_space(link_list):
    tfidf_vectorizer = TfidfVectorizer(min_df=1, lowercase = True, stop_words = 'english') # min number of occurences
    
    documents = []
    for name in link_list:
        print name
        art = Wikipedia().search(name, cached = False)
        try:
            article = wiki.get_article(art)
            text = filter_text(article)
            documents.append(' '.join(text))
        except AttributeError:
            print 'no text', name
        
    print 'creating context tfidf matrix'
    tfidf_matrix = tfidf_vectorizer.fit_transform(documents)

    print tfidf_matrix.shape
    
    return tfidf_matrix, tfidf_vectorizer.vocabulary_    
   
   
def get_interpretation_vector(inverted_index, tfidf_matrix):
    interpretation_vector = inverted_index.dot(tfidf_matrix.transpose())
    #print inverted_index.shape, tfidf_matrix.transpose().shape
    print 'test interpretation vector ', interpretation_vector
    return interpretation_vector.transpose()


def cos_sim(tfidf_matrix1, tfidf_matrix2):
    result_cos =  cosine_similarity(tfidf_matrix1, tfidf_matrix2)[0][0]

    print 'cosine value: ', result_cos

    return math.degrees(math.acos(result_cos))

   
### BEGIN TEXT_SIMILARITY ###

context_flag=False

word1 = 'photography'
word2 = 'art'

art1 = Wikipedia().search(word1, cached = False)
art2 = Wikipedia().search(word2, cached = False)



print'Done searching. Start Preprocessing...'

article1 = wiki.get_article(art1)
article2 = wiki.get_article(art2)

link_list = get_link_list(art1, art2)

link_list.sort()

if len(link_list) == 0:
    print 'there are no common links! Unable to create context!'
    context_flag=False
else:
    inverted_index, context_vocabulary = get_feature_space(link_list)
    context_flag=True

print 'Getting context done.'

print 'length of article 1: ', len(article1)
print 'length of article 2: ', len(article2)
text1 = []
text2 = []

text1 = filter_text(article1)
text2 = filter_text(article2)

print 'After filtering'
print 'length of article 1: ', len(text1)
print 'length of article 2: ', len(text2)

documents = [' '.join(text1), ' '.join(text2)]
#pprint.pprint(documents)

# documents = (
# "The sky is blue",
# "The sun is bright",
# "The sun in the sky is bright",
# "We can see the shining sun, the bright sun"
# )

print'Done preprocessing. Start Computing similarity...'


tfidf_vectorizer = TfidfVectorizer(min_df=1, lowercase = True, stop_words = 'english') # min number of occurences
tfidf_matrix = tfidf_vectorizer.fit_transform(documents)
print 'Vocabulary size: ', len(tfidf_vectorizer.vocabulary_)
print tfidf_matrix.shape

print 'direct cosine similarity'

print cos_sim(tfidf_matrix[0:1], tfidf_matrix[1:2]), ' degrees.'

interpretation_vec1 = []
interpretation_vec2 = []

if context_flag:
    
    tfidf_vectorizer = TfidfVectorizer(min_df=1, lowercase = True, stop_words = 'english', vocabulary=context_vocabulary) # min number of occurences
    tfidf_matrix = tfidf_vectorizer.fit_transform(documents)
    print 'Vocabulary size ESA: ', len(tfidf_vectorizer.vocabulary_)
    print tfidf_matrix.shape

    print 'Getting interpretation vector...'
    interpretation_vec1 = get_interpretation_vector(inverted_index, tfidf_matrix[0:1])
    interpretation_vec2 = get_interpretation_vector(inverted_index, tfidf_matrix[1:2])

    print 'Getting interpretation vector. Done.'
    print 'ESA cosine similarity'
    
    print cos_sim(interpretation_vec1, interpretation_vec2), ' degrees.'
