\documentclass[a4paper,9pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[left=2cm,right=2cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage{graphicx}
\author{Piotr Bandurski 125055 \\ Department of Automatic Control}
\title{Information Retrieval Methods - Project Report}
\begin{document}
\maketitle

\section{Task Formulation}

The main task of this project was to implement and experiment with text similarity algorithms. Cosine similarity and Explicit Semantic Analysis (ESA) were given.
In this report we first introduce the cosine similarity and ESA methods in the context of text similarity analysis, then we describe the implementation of
those methods and finally we present and comment on the results of the experiments using the aforementioned methods.

\section{The Text Similarity}

In this section we go through the text similarity procedure from parametrising the documents using $tf-idf$ transfrom to computing the cosine similarity.
At the end we describe the ESA which is using cosine similarity to compute text similarity

\subsection{Parametrizing the document}

In order to compute the similarity score between two documents it is needed to represent
them using vector space model. The method that we used in this project is \textit{term frequency-inverse document frequency}
which is a very popular method to evaluate how important is a word in a document.
Essentially it allowes the document to be represented by a vector that represent the importance of a term in a document.

\subsubsection{Defining the Vocabulary}

The first step in modeling the document into a vector space is to create a dictionary of terms present in ducuments.
To do that, it is possible to simply select all terms from a document and convert it into a dimension in the vector space, but it is
obvious that some words are very frequent and present in almost every document. Moreover in text similarity
problem it is important to extract those terms that bring the information about the nature of analysed text.
Therefore, it is allowed and advised to ingore the so-called 'stop words', such as \textit{the}, \textit{is}, \textit{at}, \textit{on}, etc.

\vspace*{0.5cm}

The dictionary is defined as follows:

 \[ E(t) = \left\{ \begin{array}{ll}
     1, & \textrm{when $\textit{t}$ is $w_1$}\\
     2, & \textrm{when $\textit{t}$ is $w_2$}\\
     3, & \textrm{when $\textit{t}$ is $w_3$}\\
     \vdots & \\
     n, & \textrm{when $\textit{t}$ is $w_n$}     
\end{array} \right. \]

where $t$ is a given term, $w$ is one of the words in a document which has $n$ words, excluding 'stop words'.

Now it is possible to convert the document into a vector space where each term of the vector
is indexed as index vocabulary. The method to represent each term in the vector space
is called \textit{term frequency}

\newpage

\subsubsection{Term Frequency}

the term-frequency is nothing more than a measure of how many times the terms present in the vocabulary $\mathrm{E}(t)$.
We define the \textit{term frequency} as a counting function:

\[ tf(t,d) = \sum_{x \in d} fr(x,t) \]

where the $fr(x,t)$ is a simple function defined as follows:


 \[ fr(x,t) = \left\{ \begin{array}{ll}
     1, & \textrm{if $x=t$}\\
     0, & \textrm{otherwise}\\     
\end{array} \right. \]

Essentially, what $tf(t,d)$ returns is how many times is the term $t$ is present in the document $d$.
Finally the document vector can be created according to the following formula:

\[ \overrightarrow{v}_d = [tf(t_1,d), tf(t_2,d), ..., tf(t_n,d)] \]

Each dimension of the document vector is represented by the term of the vocabulary.
It is also very important to normalize the resulting vector to avoid creating bias towards long
documents, which would make them appear as more important than they are simply due to
a high frequency of a term in a given document.
\vspace*{0.5cm}

The main disadvantage of \textit{term frequency} is that it tends to 
scale up frequent terms and scale down rare terms which are empirically
more informative than the high frequency terms. Many experimental test have shown
that a term that occurs frequently in many documents is not a good discriminator.
The \textit{term frequency-inverse document frequency} comes to solve the problem.


\subsubsection{Term Frequency-Inverse Document Frequency}

What \textit{tf-idf} gives is how important is a word to a document in a collection,
and that's why \textit{tf-idf} incorporates local and global parameters, because
it takes into consideration not only the isolated term but also the term within
the document collection. What \textit{tf-idf} then does to solve the problem which occured in \textit{term frequency}
, is to scale down the frequent terms while scaling up the rare terms.
It assumes that a term that occurs 10 times more frequently that another isn't
10 times more important than it, that is why \textit{tf-idf} uses the logarithmic scale.

\vspace*{0.5cm}

Let us assume that wehave a corpus of documents defined as $D = {d_1, d_2, ..., d_n}$ where $n$ is the number of documents
in the corpus. The \textit{inverse document frequency} can be defined as follows:

\[ idf(t) = \log{\frac{|D|}{1+| \{d : t \in d\} |}} \]

where ${|\{d : t \in d\}|}$ is the number of documents where term $t$ appears,
when the \textit{term frequency} function satisfies $tf(t,d) \neq 0$, we're only adding $1$ into
the formula to avoid zero-division.

\vspace*{0.5cm}

The formula for the \textit{tf-idf} is the defined as:

\[ tf\textrm{-}idf(t) = tf(t,d)\times idf(t)  \]

\vspace*{0.5cm}

This formula has an important consequence: a high weight of the \textit{tf-idf} calculation
is reached when you have a high \textit{term frequency} in the given document (local parameter) and a low \textit{document frequency} 
of the term in the whole corpus (global parameter).

On the following figure we present a nice visualization of sentences (which easily can be made documents) in the $n$ dimensional
vector space:

\begin{center}
	\includegraphics[width=0.6\textwidth]{vector_space.png}
\end{center}

Now, having the document represented in a vector space it is possible to compute the similarity score.

\subsection{Cosine Similarity Score}

The cosine similarity between two vectors (or as in out case two documents on the vector space)
is a measure that calculates the cosine of the angle between them. This metric is a measurement of
orientation and not magnitude, it can be seen as a comparison between documents on a normalized space
because we're not taking into the consideration only the magnitude of each word count (\textit{tf-idf}) of each document,
but the angle between the documents. Therefore the cosine similarity is given by the following formula:

\[ \cos{\theta} = \frac{\vec{a} \cdotp \vec{b}}{\lVert \vec{a}\rVert \lVert\vec{b}\rVert} \]

Cosine similarity will generate a metric that says how related are two documents
by looking at the angle instead of magnitude, like on the figure below:

\begin{center}
	\includegraphics[width=1\textwidth]{cos.png}
\end{center}

\newpage

\subsection{Explicit Semantic Analysis}

The second method of evaluating a similarity score between two documents is \textit{Explicit Semantic Analysis}.
\textit{Explicit Semantic Analysis} (ESA) is a data-based approach to finding similarities between two text documents. 
The basic idea is that two documents are similar if the most important words in document A are strongly semantically 
related to the most important words in document B.

\vspace*{0.5cm}

To have a strong semantic relation means that two words are similar in meaning. 
For example motorbike and car are related in meaning, and so are motorbike and racing.
Those words have a common feature that 'involves or enables movement.

It is noteworthy that two documents that are semantically related may not share a single word.
One document may describe motorbikes while the other describes the history of Formula One car racing.
While using different vocabulary, in terms of their positions in semantic space
they are pretty close to one another.

But how to measure semantic relatedness? In most cases it envolves mapping the wrods into
some kind of knowledge representation.


\subsubsection{Knowledge Representation}

In this project we used Wikipedia as a representation of world knowledge.
It consists of a set of articles, and each article represents a \textit{concept} of that knowledge representation.
All concepts are linked by the categories they belong to or by the links that refer from one
concept to another, but most importantly they are linked by the use of similar words.

Concepts are described using words and sentences. These tend to be used frequently to describe similar
concepts.

\vspace*{0.5cm}

For each concept (i.e. Wikipedia article) the \textit{tf-idf} is calculated for any given word. So
a knowledge representation is a matrix where each row (or column) represents an article in the vector space.

\subsubsection{Interpretation Vector}

To build the interpretation vector for each document we want to evaluate, we first
have to represent it in the vector space. After that it is possible to represent the documents
through the knowledge representation. An interpretation vector is given by the following formula:

\[ I(t,K)  =  <K(d,t)_{m \times n}, tf\textrm{-}idf(t)_{n \times 1}> \]
where $m$ is the number of documents in a knowledge representation and $n$ is a length of a dictionary.

\vspace*{0.5cm}

Essentially, it is the inner product of a document in the space of a knowledge representation.
This vector quantifies the importance of concepts from a knowledge representation
for each document. In a final step we can calculate the cosine similarity score between
two interpretation vectors. It would therefore express the semantic relatedness between two documents using a 
value between 0 (not related at all) and 1 (identical).

\vspace*{0.5cm}

The following figure presents the overview of ESA

\begin{center}
	\includegraphics[width=.6\textwidth]{esa_overview.png}
\end{center}

\section{The Implementation}

To implement the described algorithms we used Python scripting language. It is easy to use and comes
with a number of modules which are very useful. The algorithms were implemented in a single script file.
The modules we used were:

\begin{itemize}
	\item pattern.web - for online parsing of Wikipedia articles
	\item sklearn - for feature extraction and computing cosine similarity
	\item stemming.porter2 - for article text preprocessing
\end{itemize}

It is worth describing the text processing a bit more. To preprocess each article we used the 
Porter stemming algorithm. Generally, stemming is the process for reducing inflected words to
their stem, base or root form. For example a stemmer should identify the strings such as 'cats', 'catlike' or 'catty'
as based on the root 'cat'. In our case it allowes us to group words with similar meaning into bigger groups.

Moreover, for text processing we performed the stop words and digits filtering. We assumed that it is hard to compare
the semantic relatedness for numbers such as dates, and so they were filtered out.

\vspace*{0.5cm}

Below we present how our Python script runs:

\begin{enumerate}
 \item Get the two article names and search them online.
 \item Get the links list which will be used to create the knowledge representation
 \item Create the knowledge representation after preprocessing each article in the links list
 \item Preprocess the texts of two given articles
 \item Create the vector representation for two given articles
 \item Compute the cosine similarity score between the two given articles
 \item Create two interpretation vectors for given articles
 \item Compute the cosine similarity score between the previously computed interpretation vectors
\end{enumerate}

After having the algorithms implemented we could proceed to the experimentation phase
to evaluate the correctedness of our implementation

\section{The Experiments}

Because of the limited time we performed a handful of experiments comparing the direct cosine similarity between
two Wikipedia and the ESA-based cosine similarity.

\vspace*{0.5cm}

We used different combinations of links to create the knowledge representation. These were as follows:

\begin{itemize}
	\item All common links;
	\item All unique links;
	\item All common + 25 uncommon links from each article;
	\item 25 uncommon links from each article;
\end{itemize}

If the number of links was lower we used equal number of them for both articles.

\vspace*{0.5cm}

The articles we used to evaluate both algorithms were as follows:

\begin{itemize}
	\item Tiger - Lion;
	\item Tiger - Tiger I (tank);
	\item Tiger - Sherman Tank;
	\item Tiger I (tank) - Sherman Tank;
\end{itemize}

Below we present the scores for each combination of links we used to create the knowledge representation.










\end{document}